var selectedRow = null
var l=0;
var m=0;

function onFormSubmit() {
    if (validate()) {
        var formData = readFormData();
		console.log(formData,"formData");
        if (selectedRow == null)
            insertNewRecord(formData);
        else
            updateRecord(formData);
        resetForm();
    }
}

//Getting value from the form
function readFormData() {
	var data=0;
    var formData =[{name:document.getElementById("name").value,
    dob : document.getElementById("dob").value,
    age: document.getElementById("age").value,
    course : document.getElementById("course").value,
    email : document.getElementById("email").value,
    phone : document.getElementById("phone").value,
    file :document.getElementById("file").value.replace("C:\\fakepath\\", ""),
    photo: document.getElementById("photo").value.replace("C:\\fakepath\\", "")
	}];
	formData.forEach(data1);
	function myFunction(data1) {
   data+=data1;
	}
	 return data;
}

//Insert data in new row and cell
function insertNewRecord(data) {
    var table = document.getElementById("employeeList").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.name;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.dob;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = data.age;
    cell4 = newRow.insertCell(3);
    cell4.innerHTML = data.course;
	cell5 = newRow.insertCell(4);
    cell5.innerHTML = data.email;
	cell6 = newRow.insertCell(5);
    cell6.innerHTML = data.phone;	
	cell7 = newRow.insertCell(6);
	fileData=data.file;
	console.log(fileData,"fileData");
	cell7.innerHTML =`<a href="#" onclick="document.getElementById('fil1').src=fileData;pop1()"><p></p></a>`;
	cell8 = newRow.insertCell(7);
	imgData=data.photo;
    cell8.innerHTML = `<a href="#" onclick="document.getElementById('img1').src=imgData;pop()"><b></b></a>`;
    var f=document.getElementsByTagName("P");
	var i=document.getElementsByTagName("B");
	f[l].innerHTML=data.file;							   	
	i[l].innerHTML=data.photo;
    cell9= newRow.insertCell(8);
    cell9.innerHTML = `<button onClick="onView(this)">View</button>
	                   <button onClick="onEdit(this)">Edit</button>
                       <button onClick="onDelete(this)">Delete</button>`;				  															
	l++;
	}

// Reset form after data is submitted
function resetForm() {
    document.getElementById("name").value = "";
    document.getElementById("dob").value = "";
    document.getElementById("age").value = "";
    document.getElementById("course").value = "";
    document.getElementById("email").value = "";
    document.getElementById("phone").value = "";
    document.getElementById("file").value = "";
    document.getElementById("photo").value = "";
}

//Editing data which entered in table
function onEdit(td) {
		
    selectedRow = td.parentElement.parentElement;
    document.getElementById("name").value = selectedRow.cells[0].innerHTML;
    document.getElementById("dob").value = selectedRow.cells[1].innerHTML;
    document.getElementById("age").value = selectedRow.cells[2].innerHTML;
    document.getElementById("course").value = selectedRow.cells[3].innerHTML;
    document.getElementById("email").value = selectedRow.cells[4].innerHTML;
    document.getElementById("phone").value = selectedRow.cells[5].innerHTML;
    document.getElementById("file").value = selectedRow.cells[6].innerHTML;
    document.getElementById("photo").value = selectedRow.cells[7].innerHTML;

}

//Viewing data which entered in table
function onView(td) {
    Row = td.parentElement.parentElement;
    document.getElementById("name1").value = Row.cells[0].innerHTML;
    document.getElementById("dob1").value = Row.cells[1].innerHTML;
    document.getElementById("age1").value = Row.cells[2].innerHTML;
    document.getElementById("course1").value = Row.cells[3].innerHTML;
    document.getElementById("email1").value = Row.cells[4].innerHTML;
    document.getElementById("phone1").value = Row.cells[5].innerHTML;
    document.getElementById("file3").src = Row.cells[6].innerHTML;
	document.getElementById("photo3").src = Row.cells[7].innerHTML;
	pop2();
	showTable();
}

//After editing the form values are updated
function updateRecord(formData) {
	 var x1=document.getElementsByTagName("P");
	var y1=document.getElementsByTagName("B");
    selectedRow.cells[0].innerHTML = formData.name;
    selectedRow.cells[1].innerHTML = formData.dob;
    selectedRow.cells[2].innerHTML = formData.age;
    selectedRow.cells[3].innerHTML = formData.course;
    selectedRow.cells[4].innerHTML = formData.email;
    selectedRow.cells[5].innerHTML = formData.phone;
    selectedRow.cells[6].innerHTML = `<a href="#" onclick="pop1()"><p></p></a>`;
    selectedRow.cells[7].innerHTML = `<a href="#" onclick="pop()"><b></b></a>`;
	x1[m].innerHTML=formData.file;							   
	y1[m].innerHTML=formData.photo;
	m++;
	resetForm();
}

// Deleting row
function onDelete(td) {
    if (confirm('Are you sure to delete this record ?')) {
        row = td.parentElement.parentElement;
        document.getElementById("employeeList").deleteRow(row.rowIndex);
		hideTable();
        resetForm();
    }
}

function showTable(){
document.getElementById('viewForm').style.visibility = "visible";
}

// Form validation
function validate() 
{
var name1 = document.getElementById("name");
var dob1= document.getElementById("dob");
var age1= document.getElementById("age");
var course1= document.getElementById("course");
var phone1= document.getElementById("phone");
var email1= document.getElementById("email");
var file1= document.getElementById("file");
var photo1= document.getElementById("photo");
var filePath = document.getElementById('file').value;
var photoPath = document.getElementById('photo').value;
var fileExtensions = /(\.pdf)$/i;
var photoExtensions=/(\.jpg|\.jpeg)$/i;
var nam= /^(?!^ +$)([\w -&]+)$/;
var phn= /^([0-9]{10})+$/;
var mail= /^([A-Za-z0-9_.])+\@([a-z0-9])+\.([a-z])+$/;

ag=document.getElementById("age").value;
if(name1.value.trim()=="")
{
	alert("Please Enter Your Name");
	return false;
}else if(!nam.test(name1.value))
{
	alert("Use Only Alphabets   ");
	return false;
}else if(dob1.value.trim()=="")
{
	alert("Please Enter Your Dob");
	return false;
}else if(age1.value.trim()=="")
{
	alert("Please Enter Your Age");
	return false;
}else if (ag < 0 || ag > 200)
{
	alert("Please Enter Your Age Between 0-200");
}
else if(course1.value.trim()=="")
{
	alert("Please Enter Your Course");
	return false;
}else if(email1.value.trim()=="")
{
	alert("Please Enter Your Email");
	return false;
}else if(!mail.test(email1.value)){
	alert("Please Enter Valid Email Address ");
	return false;
	}
	else if(phone1.value.trim()=="")
{
	alert("Please Enter Your Phone Number");
	return false;
}else if(!phn.test(phone1.value)){
	alert("Use Only 10 Digit Numbers   ");
	return false;
}else if(file1.value=="")
{
	alert("Please Upload Your Certificate");
	return false;
}else if(!fileExtensions.exec(filePath)){
	
	alert('Please Upload Your File In Pdf Format');
	return false;
}

else if(photo1.value=="")
{
	alert("Please Upload Your Photo");
	return false;
}else if(!photoExtensions.exec(photoPath)){
	
	alert('Please Upload Your Photo In Jpg Format');
	return false;
}
else
{
	return true;
	}
}

var modal = null
function pop() {
   if(modal === null) {
     document.getElementById("box").style.display = "block";
     modal = true
   } else {
     document.getElementById("box").style.display = "none";
     modal = null
   }
 }
 
var box = null
function pop1() {
   if(box === null) {
     document.getElementById("filebox").style.display = "block";
     box = true
   } else {
     document.getElementById("filebox").style.display = "none";
     box = null
   }
 }
 
var view = null
function pop2() {
   if(view === null) {
     document.getElementById("viewbox").style.display = "block";
     view = true
   } else {
     document.getElementById("viewbox").style.display = "none";
     view = null
   }
 }